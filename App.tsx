import { NavigationContainer } from "@react-navigation/native";
import { SafeAreaProvider } from "react-native-safe-area-context";
import AuthNavigator from "./src/navigations/auth";
import { useFonts } from "expo-font";

export default function App() {
  // const [fontsLoaded] = useFonts({
  //   DMRegular: require("./assets/fonts/Abel.ttf"),
  // });

  // if (!fontsLoaded) {
  //   return null;
  // }

  return (
    <SafeAreaProvider>
      <NavigationContainer>
        <AuthNavigator />
      </NavigationContainer>
    </SafeAreaProvider>
  );
}

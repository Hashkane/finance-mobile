import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import HomeScreen from "../../screens/home";
import Category from "../../screens/category";
import Analytics from "../../screens/analytics";
import Profile from "../../screens/profile/profile";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => {
  return (
    <Tab.Navigator screenOptions={{ headerShown: false }}>
      <Tab.Screen component={HomeScreen} name="HomeScreen" />
      <Tab.Screen component={Category} name="Category" />
      <Tab.Screen component={Analytics} name="Analytics" />
      <Tab.Screen component={Profile} name="Profile" />
    </Tab.Navigator>
  );
};

export default BottomTabNavigator;

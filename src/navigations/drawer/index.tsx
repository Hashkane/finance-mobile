import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import BottomTabNavigator from "../bottom-tab";
import DrawerContent from "../../components/drawer-content";

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator drawerContent={(props) => <DrawerContent {...props} />}>
      <Drawer.Screen name=" " component={BottomTabNavigator} />

      <Drawer.Screen name="DrawerContent" component={DrawerContent} />
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;

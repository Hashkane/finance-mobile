import { View, Text } from "react-native";
import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import SplashScreen from "../../screens/splash";
import SignIn from "../../screens/auth/signin";
import SignUp from "../../screens/auth/signup";
import ForgotPasswordScreen from "../../screens/auth/forgot-password-screen";
import ResetPasswordScreen from "../../screens/auth/reset-password-screen";
import DrawerNavigator from "../drawer";
import EditImpormation from "../../screens/profile/edit-impormation";
import EditPassword from "../../screens/profile/edit-password";
import Transaction from "../../screens/transaction";

const AuthNavigator = () => {
  const Stack = createNativeStackNavigator();

  return (
    <Stack.Navigator
      screenOptions={{ headerShown: false }}
      initialRouteName="SplashScreen"
    >
      <Stack.Screen component={SplashScreen} name="SplashScreen" />
      <Stack.Screen component={SignIn} name="SignIn" />
      <Stack.Screen component={SignUp} name="SignUp" />
      <Stack.Screen
        component={ForgotPasswordScreen}
        name="ForgotPasswordScreen"
      />
      <Stack.Screen
        component={ResetPasswordScreen}
        name="ResetPasswordScreen"
      />
      <Stack.Screen component={DrawerNavigator} name="DrawerNavigator" />
      <Stack.Screen
        component={EditImpormation}
        name="EditImpormation"
        options={{ headerShown: true, title: "Хэрэглэгчийн мэдээлэл" }}
      />
      <Stack.Screen
        component={EditPassword}
        name="EditPassword"
        options={{ headerShown: true, title: "Нууц үг солих" }}
      />
      <Stack.Screen
        component={Transaction}
        name="Transaction"
        options={{ headerShown: true, title: "Гүйлгээ" }}
      />
    </Stack.Navigator>
  );
};

export default AuthNavigator;

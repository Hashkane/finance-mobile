const COLORS = {
  backgroundColor: "#F2F7F9",
  first: "#1361A9",
  primary: "#F2F7F9",
  secondary: "#BFD4E4",
  third: "#32C5FF",
  lightWhite: "#F2F7F9",
  white: "#FFFFFF",
  title: "#000066",
  black: "black",
  textInputBackgroundColor: "#FFFFFF",
  pink: "#F64662",
  green: "#66C109",
};
const FONT = {
  // regular: "DMRegular",
};
const SIZES = {
  xSmall: 10,
  small: 12,
  medium: 16,
  large: 20,
  xLarge: 24,
  xxLarge: 32,
};

export { COLORS, SIZES, FONT };

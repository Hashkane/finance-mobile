import home from "../../assets/images/home.svg";
import balance from "../../assets/images/balance.svg";
import category from "../../assets/images/category.svg";
import analytics from "../../assets/images/analytics.svg";

export default {
  home,
  balance,
  category,
  analytics,
};

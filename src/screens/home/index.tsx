import { View, Text, ScrollView } from "react-native";
import React from "react";
import HomeButtons from "../../components/home-buttons";
import HomeDate from "../../components/home-date";
import { NavigationProp } from "@react-navigation/native";

interface HomeScreenProps {
  navigation: NavigationProp<any, any>; // adjust the type as per your navigation library
}
const HomeScreen: React.FC<HomeScreenProps> = ({ navigation }) => {
  return (
    <View>
      <HomeDate />
      <HomeButtons navigation={navigation} />
    </View>
  );
};

export default HomeScreen;

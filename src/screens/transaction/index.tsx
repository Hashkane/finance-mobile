import { View, Text, Switch, TouchableOpacity, StyleSheet } from "react-native";
import React, { useState } from "react";
import Income from "../../components/income";
import { COLORS, FONT, SIZES } from "../../constants";

import Expense from "../../components/expense";

const Transaction = () => {
  const [selectedTab, setSelectedTab] = useState(0);

  return (
    <View style={styles.container}>
      <View style={styles.containContent}>
        <TouchableOpacity
          style={[
            styles.tabButton,
            selectedTab === 0 && styles.selectedTabButton,
          ]}
          onPress={() => setSelectedTab(0)}
        >
          <Text
            style={[
              styles.tabText,
              selectedTab === 0 && styles.selectedTabText,
            ]}
          >
            Зарлага
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.tabButton,
            selectedTab === 1 && styles.selectedTabButton,
          ]}
          onPress={() => setSelectedTab(1)}
        >
          <Text
            style={[
              styles.tabText,
              selectedTab === 1 && styles.selectedTabText,
            ]}
          >
            Орлого
          </Text>
        </TouchableOpacity>
      </View>
      {selectedTab === 0 ? (
        <View style={styles.contentContainer}>
          <Expense />
        </View>
      ) : (
        <View style={styles.contentContainer}>
          <Income />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: COLORS.lightWhite,
  },
  containContent: {
    flexDirection: "row",
    marginBottom: 20,
    backgroundColor: "#fff",
    borderRadius: 30,
  },
  tabButton: {
    width: "50%",
    height: 30,
    borderRadius: 30,
    justifyContent: "center",
    alignItems: "center",
  },
  selectedTabButton: {
    backgroundColor: COLORS.secondary,
  },
  tabText: {
    color: "#000",
  },
  selectedTabText: {
    color: "#fff",
  },
  contentContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default Transaction;

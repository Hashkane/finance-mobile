import React from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  ImageBackground,
  StyleSheet,
} from "react-native";
import { COLORS, FONT, SIZES } from "../../../constants";
import { NavigationProp } from "@react-navigation/native";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import Button from "../../../components/button";
import TextButton from "../../../components/text-button";
type ForgotProps = {
  navigation: NavigationProp<any, any>;
};
const schema = yup.object().shape({
  gmail: yup.string().email().required(),
});
function ForgotPasswordScreen({ navigation }: ForgotProps) {
  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = (data: any) => {
    alert(JSON.stringify(data));
    console.log(data);
    navigation.navigate("ResetPasswordScreen");
  };
  return (
    <View style={styles.container}>
      <Image
        source={require("../../../../assets/images/profileIcon21.png")}
        style={styles.logo}
      />
      <Text style={styles.headerTitle}>Нууц үг сэргээх</Text>
      <View style={styles.inputView}>
        <Controller
          control={control}
          rules={{
            required: true,
          }}
          render={({ field: { onChange, onBlur, value } }) => (
            <TextInput
              style={styles.inputStyle}
              placeholder="И-мэйл хаягаа оруулна уу"
              placeholderTextColor={COLORS.secondary}
              onBlur={onBlur}
              onChangeText={onChange}
              value={value}
            />
          )}
          name="gmail"
        />
      </View>
      {errors.gmail && (
        <Text style={styles.reqText}>Заавал бөглөх талбар.</Text>
      )}
      <Button title="Илгээх" onPress={handleSubmit(onSubmit)} />
      <TextButton title="Буцах" onPress={() => navigation.navigate("SignIn")} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  logo: {
    width: 100,
    height: 100,
    marginTop: 50,
    marginBottom: 20,
    alignSelf: "center",
  },
  headerTitle: {
    fontSize: SIZES.xLarge,
    color: COLORS.secondary,
    // fontFamily: FONT.regular,
    marginBottom: 15,
    alignSelf: "center",
  },
  inputView: {
    backgroundColor: COLORS.white,
    borderRadius: 30,
    width: "88%",
    height: 50,
    marginTop: 20,
    justifyContent: "center",
    padding: 10,
  },
  inputStyle: {
    marginLeft: "3%",
    height: 50,
    flex: 1,
  },
  reqText: {
    marginRight: "50%",
    color: "#FC0404",
    marginBottom: -19.5,
  },
});

export default ForgotPasswordScreen;

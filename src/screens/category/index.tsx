import React, { useState } from "react";
import { View, StyleSheet, Text, TextInput, FlatList } from "react-native";
// import { Button } from "react-native-paper";

interface CategoryItem {
  id: string;
  title: string;
}

const categories: CategoryItem[] = [
  {
    id: "1",
    title: "Цалин",
  },
  {
    id: "2",
    title: "Хадгаламж",
  },
  {
    id: "3",
    title: "Бонус",
  },
];

const Category: React.FC = () => {
  const [searchText, setSearchText] = useState("");

  const handleSearchTextChange = (text: string) => {
    setSearchText(text);
  };

  const filteredCategories = categories.filter((category) =>
    category.title.toLowerCase().includes(searchText.toLowerCase())
  );

  return (
    <View>
      <View style={styles.searchView}>
        <TextInput
          style={styles.textInputStyle}
          placeholder="Хайх"
          onChangeText={handleSearchTextChange}
          value={searchText}
        />
      </View>
      <FlatList
        data={filteredCategories}
        renderItem={({ item }) => (
          <Text style={styles.categoryItem}>{item.title}</Text>
        )}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  searchView: {
    padding: 10,
  },
  textInputStyle: {
    height: 40,
    borderColor: "gray",
    borderWidth: 1,
    paddingLeft: 10,
  },
  categoryItem: {
    padding: 10,
    fontSize: 16,
    borderBottomWidth: 1,
    borderBottomColor: "lightgray",
  },
});

export default Category;

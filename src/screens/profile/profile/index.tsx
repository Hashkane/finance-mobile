import {
  View,
  Image,
  TextInput,
  Text,
  TouchableOpacity,
  StyleSheet,
} from "react-native";
import React from "react";
import { NavigationProp } from "@react-navigation/native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { COLORS } from "../../../constants";

type SignInProps = {
  navigation: NavigationProp<any, any>;
};

const Profile = ({ navigation }: SignInProps) => {
  return (
    <View style={styles.container}>
      <View style={styles.headerView}>
        <Image
          source={require("../../../../assets/images/profileIcon21.png")}
          style={styles.profileIcon}
        ></Image>
        <View>
          <Text style={styles.username}>Username</Text>
          <Text style={styles.gmailText}>mail@gmail.com</Text>
        </View>
      </View>
      <View>
        <TextInput
          style={styles.inputBio}
          placeholder="Bio"
          placeholderTextColor={COLORS.secondary}
        />
      </View>
      <View style={styles.infoView}>
        <TouchableOpacity
          onPress={() => navigation.navigate("EditImpormation")}
        >
          <View style={styles.userInfoSection}>
            <Icon name="account-edit" size={25}></Icon>
            <Text style={styles.userInfoButtonText}>
              Хувийн мэдээлэл шинэчлэх
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("EditPassword")}>
          <View style={styles.userInfoSection}>
            <Icon name="lock-reset" size={25}></Icon>
            <Text style={styles.userInfoButtonText}>Нууц үг солих</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {}}>
          <View style={styles.userInfoSection}>
            <Icon name="help-circle-outline" size={25}></Icon>
            <Text style={styles.userInfoButtonText}>Тусламж</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {}}>
          <View style={styles.userInfoSection}>
            <Icon name="exit-to-app" size={25}></Icon>
            <Text style={styles.userInfoButtonText}>Гарах</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    margin: 0,
  },
  profileIcon: {
    width: 100,
    height: 100,
  },
  infoView: {
    marginLeft: "5%",
    justifyContent: "center",
  },
  headerView: {
    borderBottomColor: COLORS.black,
    paddingBottom: 30,
    borderBottomWidth: 1,
    flexDirection: "row",
    marginTop: "7%",
    marginRight: 20,
    marginLeft: 20,
    alignItems: "center",
  },
  username: {
    fontSize: 25,
    marginLeft: 15,
  },
  gmailText: {
    marginLeft: 15,
  },
  userInfoSection: {
    flexDirection: "row",
    // marginTop: "5%",
  },

  userInfoButtonText: {
    paddingTop: 5,
    marginLeft: 15,
  },
  inputBio: {
    margin: "5%",
  },
  bio: {
    marginTop: "5%",
  },
});
export default Profile;

import React from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  StyleSheet,
} from "react-native";
import { NavigationProp } from "@react-navigation/native";
import { useForm, Controller } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { COLORS, FONT, SIZES } from "../../../constants";
import Button from "../../../components/button";
import TextButton from "../../../components/text-button";

const schema = yup.object().shape({
  oldPassword: yup.string().min(8).max(32).required(),
  password: yup.string().min(8).max(32).required(),
  rePassword: yup.string().min(8).max(32).required(),
});

type SignInProps = {
  navigation: NavigationProp<any, any>;
};

function EditPassword({ navigation }: SignInProps) {
  const {
    reset,
    register,
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });
  const onSubmit = () => navigation.navigate("DrawerNavigator");
  return (
    <View style={styles.container}>
      <View style={styles.inputView}>
        <Controller
          control={control}
          rules={{
            required: true,
          }}
          render={({ field: { onChange, onBlur, value } }) => (
            <TextInput
              style={styles.inputStyle}
              placeholder="Хуучин нууц үгээ оруулна уу?"
              placeholderTextColor={COLORS.secondary}
              onBlur={onBlur}
              onChangeText={onChange}
              value={value}
            />
          )}
          name="oldPassword"
        />
      </View>
      {errors.oldPassword && (
        <Text style={styles.reqText}>Заавал бөглөх талбар.</Text>
      )}
      <View style={styles.inputView}>
        <Controller
          control={control}
          rules={{
            required: true,
          }}
          render={({ field: { onChange, onBlur, value } }) => (
            <TextInput
              style={styles.inputStyle}
              placeholder="Шинэ нууц үгээ оруулна уу?"
              placeholderTextColor={COLORS.secondary}
              onBlur={onBlur}
              onChangeText={onChange}
              value={value}
            />
          )}
          name="password"
        />
      </View>
      {errors.password && (
        <Text style={styles.reqText}>Заавал бөглөх талбар.</Text>
      )}
      <View style={styles.inputView}>
        <Controller
          control={control}
          rules={{
            required: true,
          }}
          render={({ field: { onChange, onBlur, value } }) => (
            <TextInput
              style={styles.inputStyle}
              placeholder="Нууц үгээ давтаж оруулна уу?"
              placeholderTextColor={COLORS.secondary}
              onBlur={onBlur}
              onChangeText={onChange}
              value={value}
            />
          )}
          name="rePassword"
        />
      </View>
      {errors.rePassword && (
        <Text style={styles.reqText}>Заавал бөглөх талбар.</Text>
      )}
      <Button title="Хадгалах" onPress={handleSubmit(onSubmit)} />
      <TextButton
        title="Буцах"
        onPress={() => navigation.navigate("DrawerNavigator")}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  logo: {
    width: 100,
    height: 100,
    marginTop: 50,
    marginBottom: 20,
    alignSelf: "center",
  },
  headerTitle: {
    fontSize: SIZES.xLarge,
    color: COLORS.secondary,
    // fontFamily: FONT.regular,
    marginBottom: 15,
    alignSelf: "center",
  },
  inputView: {
    backgroundColor: COLORS.white,
    borderRadius: 30,
    width: "88%",
    height: 50,
    marginTop: 20,
    justifyContent: "center",
    padding: 10,
  },
  inputStyle: {
    marginLeft: "3%",
    height: 50,
    flex: 1,
  },
  reqText: {
    marginRight: "50%",
    color: "#FC0404",
    marginBottom: -19.5,
  },
});

export default EditPassword;

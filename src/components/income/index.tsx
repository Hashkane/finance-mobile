import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Switch,
  Modal,
  TextInput,
} from "react-native";
import React, { useState } from "react";
import { COLORS, FONT, SIZES } from "../../constants/index";
import { Calendar } from "react-native-calendars";
import Button from "../../components/button";
import CalendarModal from "../../components/calendar-modal";

const Analytics = () => {
  const [isEnabled, setIsEnabled] = useState(false);
  const [showCalendar, setShowCalendar] = useState(false);
  const [selectedDate, setSelectedDate] = useState<string | null>(null);
  const [price, setPrice] = useState("");
  const [date, setDate] = useState(Date.now());

  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);

  const handleShowCalendar = () => {
    setShowCalendar(true);
  };

  const handleCloseCalendar = () => {
    setShowCalendar(false);
  };

  const handlePriceChange = (value: React.SetStateAction<string>) => {
    setPrice(value);
  };
  return (
    <View style={styles.container}>
      <View>
        <View style={styles.headRow}>
          <Text style={styles.headerCell}>Орлогын мэдээлэл</Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.firstCell}>Он сар</Text>
          <Text style={styles.cell}>{selectedDate || "Он сар сонгоогүй"}</Text>
          <TouchableOpacity onPress={handleShowCalendar}>
            <Text>Цааш</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.row}>
          <Text style={styles.firstCell}>Дүн</Text>
          {/* <TouchableOpacity>
            <Text>Tsaash</Text>
          </TouchableOpacity> */}
          {/* <Text style={styles.cell}>{price || "Дүн оруулах"}</Text> */}

          <TextInput
            style={styles.cell}
            value={price}
            onChangeText={handlePriceChange}
            keyboardType="numeric"
            placeholder="0"
          />
          <TouchableOpacity>
            <Text>Цааш</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.row}>
          <Text style={styles.firstCell}>Категори</Text>
          <Text style={styles.cell}>Категори сонгогдоогүй байна</Text>
          <TouchableOpacity>
            <Text>Цааш</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.row}>
          <Text style={styles.firstCell}>Тэмдэглэл</Text>
          <Text style={styles.cell}>Тэмдэглэл сонгогдоогүй байна</Text>
          <TouchableOpacity>
            <Text>Цааш</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View>
        <View style={styles.interval}>
          <Text>Давтамж</Text>
          <Switch
            trackColor={{ false: "#767577", true: "#81b0ff" }}
            thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
            ios_backgroundColor="#3e3e3e"
            onValueChange={toggleSwitch}
            value={isEnabled}
          />
        </View>
      </View>
      <View style={styles.signInButton}>
        <Button title={"Хадгалах"} onPress={() => alert("Хадгалах ажиллав")} />
      </View>
      <CalendarModal
        showCalendar={showCalendar}
        onPress={handleCloseCalendar}
        onDaySelect={(day) => {
          console.log(`Date selected: ${day.dateString}`),
            setDate(day.dateString);
        }}
      />

      <Modal visible={showCalendar} animationType="slide" transparent={true}>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <TouchableOpacity
              style={styles.closeButton}
              onPress={handleCloseCalendar}
            >
              <Text style={styles.closeButtonText}>Close</Text>
            </TouchableOpacity>
            <Calendar onDayPress={(day) => setSelectedDate(day.dateString)} />
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#eee",
    borderColor: "#666666",
    margin: 10,
    padding: 5,
    width: "100%",
  },
  headRow: {
    flexDirection: "row",
    backgroundColor: "#BFD4E4",
    padding: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  row: {
    flexDirection: "row",
    padding: 5,
    borderBottomWidth: 1,
  },
  headerCell: {
    flex: 1,
    width: 100,
    color: "white",
    height: 30,
  },
  cell: {
    flex: 1,
    width: 100,
  },
  firstCell: {
    flex: 1,
    width: 100,
  },
  signInButton: {
    alignItems: "center",
  },
  interval: {
    flexDirection: "row",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  modalContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  modalContent: {
    backgroundColor: "white",
    borderRadius: 10,
    padding: 20,
    width: "80%",
  },
  closeButton: {
    alignSelf: "flex-end",
  },
  closeButtonText: {
    fontSize: 16,
    fontWeight: "bold",
  },
});

export default Analytics;

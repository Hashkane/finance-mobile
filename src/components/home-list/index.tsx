import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
} from "react-native";
import { COLORS } from "../../constants";

interface dataItem {
  income: {
    id: number;
    name: string;
    total_amount: number;
  }[];
  expense: {
    id: number;
    name: string;
    total_amount: number;
  }[];
  balance: number;
  incomeSum: number;
  expenceSum: number;
}

type Props = {
  data: dataItem;
};
const HomeList: React.FC<Props> = ({ data }) => {
  return (
    <View style={styles.container}>
      <View style={styles.titleView}>
        <Text style={styles.titleText}>Нийт орлого</Text>
        <Text style={styles.rowIncomeSumText}>{data.incomeSum}₮</Text>
      </View>
      <ScrollView>
        {data.income.map((item) => (
          <View key={item.id} style={styles.row}>
            <Text style={styles.rowText}>{item.name}</Text>
            <Text style={styles.rowIncomeText}>{item.total_amount}₮</Text>
          </View>
        ))}
      </ScrollView>
      <View style={styles.titleView}>
        <Text style={styles.titleText}>Нийт зарлага</Text>
        <Text style={styles.rowExpenceSumText}>-{data.expenceSum}₮</Text>
      </View>
      <ScrollView>
        {data.expense.map((item) => (
          <View key={item.id} style={styles.row}>
            <Text style={styles.rowText}>{item.name}</Text>
            <Text style={styles.rowExpenceText}>{item.total_amount}₮</Text>
          </View>
        ))}
      </ScrollView>
      <View style={styles.balanceView}>
        <Text style={styles.titleText}>Нийт үлдэгдэл </Text>
        <Text style={styles.rowBalanceText}> {data.balance}₮</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: "73%",
  },
  row: {
    justifyContent: "space-between",
    flexDirection: "row",
    height: 40,
    alignItems: "center",
    marginHorizontal: "10%",
  },
  rowText: {
    color: COLORS.secondary,
  },
  rowIncomeText: {
    color: COLORS.green,
  },
  rowExpenceText: {
    color: COLORS.pink,
  },
  rowIncomeSumText: {
    color: COLORS.green,
    fontSize: 18,
    marginRight: "10%",
  },
  rowExpenceSumText: {
    color: COLORS.pink,
    fontSize: 18,
    marginRight: "10%",
  },
  rowBalanceText: {
    color: COLORS.third,
    fontSize: 18,
  },
  titleView: {
    backgroundColor: COLORS.white,
    justifyContent: "space-between",
    flexDirection: "row",
    height: 40,
    alignItems: "center",
  },
  balanceView: {
    backgroundColor: COLORS.white,
    height: 40,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  titleText: {
    marginLeft: "10%",
    fontSize: 17,
    color: COLORS.secondary,
  },
});

export default HomeList;

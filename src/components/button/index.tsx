import React from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import { COLORS, FONT, SIZES } from "../../constants/index";

type Props = {
  title: string;
  onPress: () => void;
};

const Button: React.FC<Props> = ({ title, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.loginButton}>
      <Text style={styles.textButton}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  loginButton: {
    height: 50,
    borderRadius: 30,
    backgroundColor: COLORS.third,
    width: "90%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: "5%",
  },
  textButton: {
    fontSize: SIZES.large,
    color: "#FFFFFF",
  },
});

export default Button;

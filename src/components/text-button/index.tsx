import React from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import { COLORS, FONT, SIZES } from "../../constants/index";

type Props = {
  title: string;
  onPress: () => void;
};

const TextButton: React.FC<Props> = ({ title, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.sentButton}>
      <Text style={styles.textButton}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  sentButton: {
    marginTop: "5%",
    fontSize: SIZES.medium,
    color: COLORS.third,
    height: 50,
    borderRadius: 30,
    marginBottom: -20,
  },
  textButton: {
    color: "#32C5FF",
  },
});

export default TextButton;

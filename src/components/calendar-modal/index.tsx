import React from "react";
import { useState, useMemo } from "react";
import { View, Text, TouchableOpacity, StyleSheet, Modal } from "react-native";
import { Calendar } from "react-native-calendars";
import { COLORS, FONT, SIZES } from "../../constants/index";

type Props = {
  showCalendar: boolean;
  onPress: () => void;
  onDaySelect: (day: any) => void;
};

const CalendarModal: React.FC<Props> = ({
  showCalendar,
  onPress,
  onDaySelect,
}) => {
  const currentDate = new Date(Date.now());
  const [date, setDate] = useState(currentDate.toISOString().split("T")[0]);
  const maxDate = new Date(currentDate);
  const minDate = new Date(currentDate);
  minDate.setMonth(minDate.getMonth() - 1);

  const selected = useMemo(
    () => ({
      [date]: { selected: true },
    }),
    [date]
  );

  return (
    <Modal visible={showCalendar} animationType="slide" transparent={true}>
      <View style={styles.modalContainer}>
        <View style={styles.modalContent}>
          <TouchableOpacity style={styles.closeButton} onPress={onPress}>
            <Text style={styles.closeButtonText}>Close</Text>
          </TouchableOpacity>
          <Calendar
            initialDate={date}
            minDate={minDate.toISOString().split("T")[0]}
            maxDate={maxDate.toISOString().split("T")[0]}
            markedDates={selected}
            onDayPress={(day) => {
              setDate(day.dateString);
              onDaySelect && onDaySelect(day);
            }}
          />
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  modalContent: {
    backgroundColor: "white",
    borderRadius: 10,
    padding: 20,
    width: "80%",
  },
  closeButton: {
    alignSelf: "flex-end",
  },
  closeButtonText: {
    fontSize: 16,
    fontWeight: "bold",
  },
});

export default CalendarModal;

import React from "react";
import { View, Text, TouchableOpacity, StyleSheet, Modal } from "react-native";
import { COLORS, FONT, SIZES } from "../../constants/index";

type Props = {
  incomeSum: number;
  expenceSum: number;
};

const HomeBalance: React.FC<Props> = ({ incomeSum, expenceSum }) => {
  const income = (incomeSum / (incomeSum + expenceSum)) * 100;
  const expense = (expenceSum / (incomeSum + expenceSum)) * 100;

  return (
    <View style={styles.container}>
      <View
        style={[
          styles.incomeBalance,
          { width: `${income}%`, backgroundColor: COLORS.green },
        ]}
      ></View>
      <View
        style={[
          styles.expenceBalance,
          { width: `${expense}%`, backgroundColor: COLORS.pink },
        ]}
      ></View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginVertical: "3%",
    height: "5%",
    flexDirection: "row",
    justifyContent: "center",
    width: "85%",
    alignSelf: "center",
  },
  incomeBalance: {
    height: "20%",
    width: "40%",
    borderRadius: 30,
    alignSelf: "center",
  },
  expenceBalance: {
    height: "20%",
    width: "40%",
    borderRadius: 30,
    alignSelf: "center",
  },
});

export default HomeBalance;

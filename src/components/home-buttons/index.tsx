import React from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import { COLORS, FONT, SIZES } from "../../constants/index";
import { NavigationProp } from "@react-navigation/native";

type HomeButtons = {
  navigation: NavigationProp<any, any>;
};

const HomeButtons = ({ navigation }: HomeButtons) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => navigation.navigate("Transaction")}
        style={styles.expButton}
      >
        <Text style={styles.buttonText}>Зарлага</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => navigation.navigate("Transaction")}
        style={styles.incomeButton}
      >
        <Text style={styles.buttonText}>Орлого</Text>
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    position: "absolute",
    height: "100%",
    width: "100%",
    justifyContent: "space-around",
    flexDirection: "row",
    marginTop: "140%",
  },
  incomeButton: {
    height: 60,
    borderRadius: 30,
    backgroundColor: COLORS.green,
    width: "45%",
    alignItems: "center",
    justifyContent: "center",
  },
  expButton: {
    height: 60,
    borderRadius: 30,
    backgroundColor: COLORS.pink,
    width: "45%",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonText: {
    fontSize: SIZES.large,
    color: "#FFFFFF",
  },
});

export default HomeButtons;

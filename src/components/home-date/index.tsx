import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  Modal,
} from "react-native";
import { useState, useEffect } from "react";
import { COLORS, FONT, SIZES } from "../../constants/index";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import HomeList from "../home-list";
import HomeBalance from "../home-balance";
import CalendarModal from "../calendar-modal";
const incomesExpense = {
  income: [
    {
      id: 1,
      name: "Гэр ахуй",
      total_amount: 50000,
    },
    {
      id: 2,
      name: "Хоол",
      total_amount: 60000,
    },
    {
      id: 3,
      name: "Хувцас",
      total_amount: 450000,
    },
  ],
  expense: [
    {
      id: 4,
      name: "Гэр ахуй",
      total_amount: 30000,
    },
    {
      id: 5,
      name: "Party",
      total_amount: 300000,
    },
    {
      id: 6,
      name: "Гэр ахуй",
      total_amount: 30000,
    },
    {
      id: 7,
      name: "Party",
      total_amount: 300000,
    },
    {
      id: 8,
      name: "Гэр ахуй",
      total_amount: 30000,
    },
    {
      id: 9,
      name: "Party",
      total_amount: 300000,
    },
    {
      id: 10,
      name: "Гэр ахуй",
      total_amount: 30000,
    },
    {
      id: 11,
      name: "Party",
      total_amount: 300000,
    },
    {
      id: 12,
      name: "Гэр ахуй",
      total_amount: 30000,
    },
    {
      id: 13,
      name: "Party",
      total_amount: 300000,
    },
  ],
  balance: 560000,
  incomeSum: 1200000,
  expenceSum: 640000,
};
const HomeDate = () => {
  const [data, setData] = useState(incomesExpense);

  const getData = async () => {
    // const response = await fetch(url);
    // const value = await response.json();
    setData(incomesExpense);
    console.log("Amjilttai");
  };

  const [date, setDate] = useState(Date.now());
  const [strDate, setStrDate] = useState("");
  useEffect(() => {
    const currentDate = new Date(date);
    setStrDate(
      currentDate.getMonth() +
        1 +
        " сар " +
        currentDate.getDate() +
        ", " +
        currentDate.getFullYear()
    );
    getData();
  }, [date]);
  const changeDate = () => {
    handleShowCalendar();
  };
  const addDate = () => {
    const currentDate = new Date(date);
    const nextDate = new Date(currentDate.setDate(currentDate.getDate() + 1));
    setDate(nextDate.getTime());
  };
  const divDate = () => {
    const currentDate = new Date(date);
    const nextDate = new Date(currentDate.setDate(currentDate.getDate() - 1));
    setDate(nextDate.getTime());
  };

  const [showCalendar, setShowCalendar] = useState(false);
  const handleShowCalendar = () => {
    setShowCalendar(true);
  };
  const handleCloseCalendar = () => {
    setShowCalendar(false);
  };

  return (
    <View>
      <View style={styles.dateView}>
        <TouchableOpacity onPress={divDate} style={styles.touchable}>
          <Icon name="less-than" size={25} style={styles.dateIcon} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.dateButton} onPress={changeDate}>
          <Text style={styles.dateText}>{strDate}</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={addDate} style={styles.touchable}>
          <Icon name="greater-than" size={25} style={styles.dateIcon} />
        </TouchableOpacity>
      </View>
      <CalendarModal
        showCalendar={showCalendar}
        onPress={handleCloseCalendar}
        onDaySelect={(day) => {
          console.log(`Date selected: ${day.dateString}`),
            setDate(day.dateString);
        }}
      />
      <HomeBalance incomeSum={data.incomeSum} expenceSum={data.expenceSum} />
      <HomeList data={data} />
    </View>
  );
};

const styles = StyleSheet.create({
  dateView: {
    justifyContent: "space-around",
    alignItems: "center",
    flexDirection: "row",
    borderBottomWidth: 1,
    marginVertical: "3%",
    paddingBottom: "3%",
    borderBottomColor: COLORS.secondary,
  },
  dateIcon: {
    color: COLORS.secondary,
  },
  dateButton: {
    height: 30,
    borderRadius: 30,
    backgroundColor: COLORS.secondary,
    width: "30%",
    alignItems: "center",
    justifyContent: "center",
  },
  dateText: {
    alignItems: "center",
    color: "#fff",
  },
  touchable: {
    height: 30,
  },
});

export default HomeDate;
